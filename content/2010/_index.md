---
title:       "Akademy 2010"
date:        2010-03-12
changed:     2010-07-19
menu:
  "2010":
    name: 2010
    weight: 1
  main:
    name: "2010"
    weight: 990
    parent: prev-years
---
<img style="float: left; padding-right: 10px;" src="/media/2010/akademy2010.png" alt="Akademy 2010 Flag Logo" width="121" height="120" />

<strong><em>Tampere, Finland 3-10th July 2010</em></strong>

Akademy is the annual world summit of the <a href="http://www.kde.org/">KDE community</a>.   The event features an interesting mix of talks and presentations by prominent Free Software developers, coding sessions, workshops, and last but not least social activities.

Thanks to our sponsors, the organising team and everyone who attended for making this event possible.

If you weren't able to attend, the videos and slides of most of the talks are <a href="./program/conference">available</a>.

Akademy 2010 was organised by <a href="http://ev.kde.org/">KDE e.V.</a> and <a href="http://www.coss.fi/">COSS</a>.
