---
title:       "Travel"
date:        2010-03-16
changed:     2010-04-06
menu:
  "2010":
    parent: travel
    weight: 3
---
<img class="img-fluid" src="/media/2010/travel.page_.png" alt="" align="center" />

<h2>By plane</h2>
<p>Tampere has direct flight connections with Stockholm, Copenhagen, Frankfurt, London, Riga, Milan, Edinburgh, and Bremen.</p>

<p>The frequent daily flights from <a href="http://www.helsinki-vantaa.fi/home">Helsinki-Vantaa Airport (HEL)</a> to <a href="http://www.finavia.fi/airport_tampere-pirkkala?airport_url=%2Flentoasema_tampere-pirkkala">Tampere-Pirkkala Airport (TMP)</a> take 30 minutes. Please note that the budget airline Ryanair flies to Tampere-Pirkkala Airport from several European cities.</p>
<p>Tampere-Pirkkala Airport is about 15 km from the city centre. The Airport Taxi can be ordered at the airport as well as at the hotel reception. There are also regular taxis waiting for passengers at the airport. If you want to order taxi beforehand, call +3581004131.</p>
<p>Public Transport Bus number 61 operates between the airport and Tampere.</p>
<h2>By train</h2>
<p>If you arrive at Helsinki-Vantaa Airport, but plan to take the train to Tampere, you must take the bus to Helsinki city centre. From the airport bus station, take a bus to Helsinki city centre or Tikkurila railway station and then board the train to Tempere there. Trains depart hourly (except for night time). The travel time by train is about 2 hours. We recommend you to buy a ticket on either a Pendolino or InterCity train.</p>
<p>Further information please visit <a href="http://www.vr.fi/eng/">http://www.vr.fi/eng/</a>. You have the option to select seat preference: for example a PC seat in an open or working compartment.</p>
<h2>By bus</h2>
<p>If you do not stay in Helsinki, it may be easier and faster to take the express bus than to take the train. The bus leaves from Helsinki-Vantaa Airport directly to Tampere. Buses depart every 1-2 hours and the travel time is approximately 2 hours 30 minutes. You may need to change the bus at Keimolanportti. Driver will lift your luggage to the other bus, and you just need to remember to walk to that bus nearby. You can ask additional information from the driver.</p>
<p>Search the <a href="http://www.matkahuolto.com/en/" data-proofer-ignore>timetable</a>.</p>
