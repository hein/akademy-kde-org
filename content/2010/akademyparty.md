---
title:       "Akademy Party"
date:        2010-03-16
changed:     2016-11-02
menu:
  "2010":
    parent: social
    weight: 2
---
<p>The Akademy 2010 party will take place on Saturday July 3rd, starting at 8 pm. The party will be held in the Love Hotel Nightclub, Hämeenkatu 10, which is located just in the city center, 200 meters away from the Student House TOAS City.</p>
<p>The party is sponsored by</p>
<p><img src="/media/2010/meego.png" alt="" /></p>

<p>We will have the whole place for ourselves until 10pm and after that the 2nd floor will be reserved only for us. On the 2nd floor there are several lounges, decorated in different styles, where you can relax and chat in a more cosy environment.</p>
<p>Please note that you will get in only with your conference badge, so be sure to take it with you. You will be offered some drink vouchers when you arrive, but as there will be no food available, remember to eat before joining the party.</p>

![](/media/2010/lovehotel_01.jpeg)
