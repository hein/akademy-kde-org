---
title:       "Highlights of KDE Women 2010"
date:        2010-05-05


---
<p><strong>Speaker: </strong>Celeste Lyn Paul</p>

<p>This talk will be a 45 minute presentation highlighting the women contributors in KDE. It will be conducted in a "lightning talk" method with 5-8 minute summaries given by each of the women. The talks will encompass a wide area of contributions such as development, community, documentation, design, promotion, etc. Approximately 10-15 minutes will be left at the end of the presentation slot to allow for comments in a "panel" format, where audience members may ask one or all of the speakers questions about their work. A technical report including summary abstracts of each speaker's presentation will also be submitted.</p>
<h2>Celeste Lyn Paul</h2>
