---
title:       "Notes on Application Integration from the KDE Finance Apps Group"
date:        2010-05-05


---
<p><strong>Speaker:</strong> Klaas Freitag</p>

<p>Starting at Akademy 2008 on Akademy in Belgium, people involved in finance related applications in KDE began to talk about how collaboration within the community could happen. Two years later, the first sprint of the group happened in Frankfurt/Germany to discuss the KDE Finance Applications Group and drive the integration in the finance domain.<br /><br />This talk gives an overview of the applications involved in KDE Finance, their roadmaps and people, and introduces their work to produce a domain-specific middleware called Alkimia to enable better integration of financial applications in KDE.<br /><br />Alkimia is a KDE service to handle financial transactions. The rationale of the framework is to provide the data and common business logic to all applications which deal with financial data.The talk will describe Alkimias usecases and some technical details.<br /><br />It aims to stop implementing financial core functionality in each and every application. For example, it does not make sense to implement accounting functionality in Kraft if there are personal finance managers designed for that. The aim is to integrate and focus on a good framework of specialized applications sharing data with each other in the finance, business and office space. With that the KDE software compilation will have another gem integrated for the good of the users.<br /><br />The talk will give an insight on the core concepts of Alkimia and will report on the current implementation status and the plan.<br /><br />From the application programmer's point of view the talk will highlight why the KDE Software Platform is a great foundation to work on and also name a few challenges.</p>
<h2>Klaas Freitag</h2>
<p><img style="float: right;" src="/media/2010/speakers/klaasfreitag.jpg" alt="" width="175" height="200" /></p>
