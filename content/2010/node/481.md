---
title:       "Open Collaboration Services"
date:        2010-05-05
changed:     2016-11-02


---
<p><strong>Speaker:</strong> Frederik Gladhorn</p>

<p>In this talk I would like to introduce the behind the scenes side of things behind openDesktop.org and libattica in KDE. Using this API the new "Get Hot New Stuff" has been implemented. Another example of its use is the "Social About Dialog" (in Amarok, hopefully in all of KDE soon). One goal of OCS API is to enable easing the creation of content, distributing it and the social aspects such as becoming a fan of it or rating it.<br /><br />Another use is to enable applications to integrate features that reach out to other users. Take Parley as an example: it is a vocabulary training app, but with OSC it lets you publish that you are currently learning Finish with just one click. Now it will show you other users that learn the same languages as you currently do. Send them a message with just one click to get in touch and share learning experiences.<br /><br />Other consumers of the API are the "Community" and "Social News" Plasma applets. Community enables sending messages to other users of OCS servers (currently openDesktop). Social News shows a stream of activities of other users. Its newest feature is integration of updates coming from your ownCloud installation. This enables you to get notifications when a friend accesses your cloud to download something or to be in the know when she posts you a new song in your ownCloud.</p>
<h2>Frederik Gladhorn</h2>
<p><img style="float: right;" src="/media/2010/speakers/Frederik_Gladhorn.JPG" alt="" />Frederik Gladhorn has been a long term KDE user before becoming involved in  development in 2006. He started by improving his Spanish vocabulary, which lead to patches for  KVocTrain. Turning KVocTrain into Parley is an ongoing process and a lot of  fun with the KDE Education people.  Lately Frederik worked on parts of the Social Desktop project and KDE's Get  Hot New Stuff implementation.</p>
