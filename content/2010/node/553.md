---
title:       "KDE's wikis - getting closer to world domination with SMW and SMW+"
date:        2010-05-05
changed:     2016-11-02


---
<p><strong>Speaker:</strong> Lydia Pintscher</p>

<p>KDE relies heavily on its three wikis - Userbase, Techbase and Communitybase - when it comes to collaboration. We plan releases, sprints and attendance to conferences, we provide documentation, guidelines, how-tos and meeting notes and much much more there. However our wikis could offer so much more by enabling semantic annotations and making use of them using the MediaWiki extensions Semantic MediaWiki and SMW+. This talk will show some of the areas where each of KDE's wikis could gain and how it would help the KDE community be more productive.</p>
<h2>Lydia Pintscher</h2>
<p><img style="float: right;" src="/media/2010/speakers/lydia.jpg" alt="" width="200" height="150" />Lydia is a 25 year old student of computer science at the university of Karlsruhe with a focus on medicine and biosignals. Her contributions to KDE include being a member of the Community Working Group, community and release manager for Amarok, GSoC admin, doing promo work and generally picking up stuff that needs to get done.</p>
