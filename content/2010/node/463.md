---
title:       "KDE performance again ... and again ... and again"
date:        2010-05-05
changed:     2016-11-02


---
<p><strong>Speaker:</strong> Luboš Luňák</p>

<p>KDE workspace and applications often have a reputation of having extensive resource usage, but these claims usually come without any evidence other than "feelings", questionable benchmarks or other similar claims. When KDE performance is discussed, the discussion rarely includes reliable numbers or tests that could be easily reproduced. As such there are almost no reliable public data on KDE's performance. The talk will discuss basic metrics for measuring performance and resource usage and avoiding the usual problems when using them, their relevance for KDE and various scenarios, and how they should be used to create a KDE performance testcase that would provide a consistent and reproducible way of measuring KDE's performance.</p>
<h2>Luboš Luňák</h2>
<p><img style="float: right;" src="/media/2010/speakers/luboslunak.jpg" alt="" width="160" height="200" />Lubos Lunak is a KDE developer employed by SUSE/Novell, where he works on the openSUSE distribution as a member of the openSUSE Boosters team and the KDE team. In KDE he has worked on areas such as the window manager KWin or performance improvements.</p>
