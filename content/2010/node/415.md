---
title:       "I can see your house from here"
date:        2010-05-05
changed:     2016-11-02


---
<p><strong>Speakers:</strong> Till Adam and Marius Bugge Monsen</p>

<p>The model/view framework is one of the most powerful areas of Qt4, but also one of the most complex. It is elegant in its design, but proves to be difficult to master for many who try in practice. While a lot of effort went into anticipating how developers would use and potentially mis-use this API, real world usage patterns, best practices and common errors turn out to be interestingly deviant. Marius, who was part of the original architecture team behind these classes, and Till, who has taught the concepts to varied audiences and seen a wide range of real world applications of them both in his work on KDEPIM and as a consultant at KDAB, combine their experiences in this presentation to provide a unique joint perspective.</p>
<h2>Till Adam</h2>
<p><img style="float: right;" src="/media/2010/speakers/tilladam.jpg" alt="" width="200" height="150" />Till contributes to KDEPIM and Akonadi by mostly staying out of the way of those doing the actual work, these days. In return, they let him fly around the world to tell everyone, everywhere about how cool this stuff is. Part of his job at KDAB consists of the coordination of the company's KDE related activities. He lives in Berlin with his wife and daughter.</p>
<h2 style="clear: both;">Marius Bugge Monsen</h2>
<p><img style="float: right;" src="/media/2010/speakers/marius.jpeg" alt="" width="200" height="150" />Marius is a Senior Software Engineer at Nokia, Qt Development Frameworks and leads the Qt Widgets Team. He holds a M.Sc. in computer science from the Norwegian University of Science and Technology (NTNU). When he is not working on new Qt widget architectures he enjoys climbing and lazy weekends in bed.</p>
