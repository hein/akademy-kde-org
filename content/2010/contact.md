---
title:       "Contact"
date:        2010-03-16
changed:     2010-04-07
menu:
  "2010":
    weight: 7
---
Akademy 2010 is organised by <a href="http://ev.kde.org">KDE e.V.</a> and <a href="http://www.coss.fi/en">COSS</a>.

To contact the Akademy organising team please email akademy-team@kde.org or for other matters contact the board of the KDE e.V at kde-ev-board@kde.org.

The IRC channel for the event is #akademy on Libera Chat, you are welcome to sit in the channel or just pop in and ask questions, but please remember you might not instantly get an answer so stay for a reasonable period.
