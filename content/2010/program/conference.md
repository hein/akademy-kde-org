---
title:       "Conference Program"
date:        2010-05-05
changed:     2010-08-10
menu:
  "2010":
    parent: program
    name: Presentations
    weight: 3
scssFiles:
- /scss/2010/images.scss
- /scss/tables.scss
---
<table id="sched_sat" class="schedule">
<tbody>
</tbody>
<tbody>
<tr>
<th colspan="3">Saturday, 3 July 2010</th>
</tr>
<tr>
<th>Time</th><th>Room 1</th><th>Room 2</th>
</tr>
<tr>
<td>9:30 - 9:45</td>
<td class="notrack" colspan="2">Opening<br /><a href="http://files.kde.org/akademy/2010/videos/Akademy_Opening.ogv"><img src="/media/2010/video.png" /></a></td>
</tr>
<tr>
<td>9:45 - 10:30</td>
<td class="track_keynote" colspan="2"><strong>Keynote</strong><br /><a href="/2010/node/571">MeeGo redefining the Linux desktop landscape</a>
<br /><em>Valtteri Halla (Nokia)</em><br /><a href="http://files.kde.org/akademy/2010/videos/MeeGo_redefining_the_Linux_desktop_landscape-Valtteri_Halla.ogv"><img src="/media/2010/video.png" /></a></td>
</tr>
<tr>
<td>&nbsp;</td>
<td class="track1"><strong>Community</strong></td>
<td class="track2"><strong>Mobile</strong></td>
</tr>
<tr>
<td>10:30 - 11:15</td>
<td class="track1"><a href="/2010/node/307">The 7 principles of successful open source communities</a><br /><em>Thomas Thym</em><br /><a href="http://files.kde.org/akademy/2010/slides/The_7_principles_of_successful_open_source_communities-Thomas_Thym.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/The_7_principles_of_successful_open_source_communities-Thomas_Thym.ogv"><img src="/media/2010/video.png" /></a></td>
<td class="track2"><a href="/2010/node/313">Communication as a Service - what Telepathy is bringing to the KDE Software Compilation</a><br /><em>George Goldberg</em><br /><a href="http://files.kde.org/akademy/2010/slides/Communication_as_a_Service_-_what_Telepathy_is_bringing_to_the_KDE_Software_Compilation-George_Goldberg.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/Communication_as_a_Service_what_Telepathy_is_bringing_to_the_KDE_Software_Compilation-George_Goldberg.ogv"><img src="/media/2010/video.png" /></a></td>
</tr>
<tr>
<td>11:15 - 11:45</td>
<td class="track1"><a href="/2010/node/319">KDE Forums: Reaching out to the community</a><br /><em>Ingo Malchow and Nils Adermann</em><br /><a href="http://files.kde.org/akademy/2010/slides/KDE_Forums_Reaching_out_to_the_community-Ingo_Malchow+Nils_Adermann.pdf"><img src="/media/2010/pdf.png" /></a></td>
<td class="track2"><a href="/2010/node/325">How to get your Qt based application on the OVI store</a><br /><em>Espen Riskedal</em><br /><a href="http://files.kde.org/akademy/2010/slides/How_to_get_your_Qt_based_application_on_the_OVI_store-Espen_Riskedal.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/How_to_get_your_Qt_based_application_on_the_OVI_store-Espen_Riskedal.ogv"><img src="/media/2010/video.png" /></a></td>
</tr>
<tr>
<td>11:45 - 12:00</td>
<td class="notrack" colspan="2">Break</td>
</tr>
<tr>
<td>12:00 - 12:45</td>
<td class="track1"><a href="/2010/node/331">Highlights of KDE Women 2010</a><br /><em>Alexandra Leisse, Camila Ayres, Celeste Lyn Paul, Lydia Pintscher & Anne Wilson</em><br /><a href="http://files.kde.org/akademy/2010/slides/Highlights_of_KDE_Women_2010-Celeste_Lyn_Paul.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/Highlights_of_KDE_Women_2010-Celeste_Lyn_Paul.ogv"><img src="/media/2010/video.png" /></a></td>
<td class="track2"><a href="/2010/node/337">Styles in Qt and KDE: A new approach</a><br /><em>Eduardo Madeira Fleury</em><br /><a href="http://files.kde.org/akademy/2010/slides/Styles_in_Qt_and_KDE_A_new_approach-Eduardo_Madeira_Fleury.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/Styles_in_Qt_and_KDE_A_new_approach-Eduardo_Madeira_Fleury.ogv"><img src="/media/2010/video.png" /></a></td>
</tr>
<tr>
<td>12:45 - 13:15</td>
<td class="track1"><a href="/2010/node/343">Wikimedia User Experience programs: lowering the barriers of entry</a><br /><em>Guillaume Paumier and Parul Vora</em><br /><a href="http://files.kde.org/akademy/2010/slides/Wikimedia_User_Experience_programs_lowering_the_barriers_of_entry-Guillaume_Paumier+Parul_Vora%0A.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/Wikimedia_User_Experience_programs_lowering_the_barriers_of_entry-Guillaume_Paumier+Parul_Vora.ogv"><img src="/media/2010/video.png" /></a></td>
<td class="track2"><a href="/2010/node/349">Avalanche in your pocket - KDEPIM on mobile devices</a><br /><em>Till Adam</em><br /><a href="http://files.kde.org/akademy/2010/slides/Avalanche_in_your_pocket_-_KDEPIM_on_mobile_devices-Till_Adam.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/Avalanche_in_your_pocket_-_KDEPIM_on_mobile_devices-Till_Adam.ogv"><img src="/media/2010/video.png" /></a></td>
</tr>
<tr>
<td>13:15 - 13:45</td>
<td class="track1"><a href="/2010/node/355">Pimp My Community</a><br /><em>Lydia Pintscher</em><br /><a href="http://files.kde.org/akademy/2010/slides/Pimp_My_Community-Lydia_Pintscher.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/Pimp_My_Community-Lydia_Pintscher.ogv"><img src="/media/2010/video.png" /></a></td>
<td class="track2"><a href="/2010/node/361">Finding Direction - Marble goes Mobile</a><br /><em>Torsten Rahn</em><br /><a href="http://files.kde.org/akademy/2010/slides/Finding_Direction_-_Marble_goes_Mobile-Torsten_Rahn.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/Finding_Direction_-_Marble_goes_Mobile-Torsten_Rahn.ogv"><img src="/media/2010/video.png" /></a></td>
</tr>
<tr>
<td>13:45 - 15:15</td>
<td class="notrack" colspan="2">Lunch</td>
</tr>
<tr>
<td>15:15 - 16:00</td>
<td class="track1"><a href="/2010/node/367">Blurring the Boundaries of Music</a><br /><em>Leo Franchi</em><br /><a href="http://files.kde.org/akademy/2010/slides/Blurring_the_Boundaries_of_Music-Leo_Franchi.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/Blurring_the_Boundaries_of_Music-Leo_Franchi.ogv"><img src="/media/2010/video.png" /></a></td>
<td class="track2"><a href="/2010/node/373">Plasma sneaks in your pocket</a><br /><em>Artur Duque de Souza and Alexis Menard</em><br /><a href="http://files.kde.org/akademy/2010/slides/Plasma_sneaks_in_your_pocket-Artur_Duque_de_Souza+Alexis_Menard.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/Plasma_sneaks_in_your_pocket-Artur_Duque_de_Souza+Alexis_Menard.ogv"><img src="/media/2010/video.png" /></a></td>
</tr>
<tr>
<td>16:00 - 16:30</td>
<td class="track1"><a href="/2010/node/379">From Consumer to Creator - The Lego Generation in the Digital Age</a><br /><em>Nikolaj Hald Nielsen</em><br /><a href="http://files.kde.org/akademy/2010/slides/From_Consumer_to_Creator_-_The_Lego_Generation_in_the_Digital_Age-Nikolaj_Hald_Nielsen.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/From_Consumer_to_Creator_The_Lego_Generation_in_the_Digital_Age-Nikolaj_Hald_Nielsen.ogv"><img src="/media/2010/video.png" /></a></td>
<td class="track2"><a href="/2010/node/385">Enhancing User Engagement on Mobile Devices</a><br /><em>Randall Arnold</em><br /><a href="http://files.kde.org/akademy/2010/slides/Enhancing_User_Engagement_on_Mobile_Devices-Randall_Arnold.pdf"><img src="/media/2010/pdf.png" /></a></td>
</tr>
<tr>
<td>16:30 - 17:00</td>
<td class="track1"><a href="/2010/node/391">New legal grounds</a><br /><em>Adriaan de Groot</em><br /><a href="http://files.kde.org/akademy/2010/slides/New_legal_grounds-Adriaan_de_Groot.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/New_legal_grounds-Adriaan_de_Groot.ogv"><img src="/media/2010/video.png" /></a></td>
<td class="track2"><a href="/2010/node/397">Towards a location-aware desktop</a><br /><em>Henri Bergius</em><br /><a href="http://files.kde.org/akademy/2010/slides/Towards_a_location-aware_desktop-Henri_Bergius.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/Towards_a_location-aware_desktop-Henri_Bergius.ogv"><img src="/media/2010/video.png" /></a></td>
</tr>
<tr>
<td>17:00 - 17:15</td>
<td class="notrack" colspan="2">Break</td>
</tr>
<tr>
<td>&nbsp;</td>
<td class="track3"><strong>Development</strong></td>
<td class="track2">&nbsp;</td>
</tr>
<tr>
<td>17:15 - 17:45</td>
<td class="track3"><a href="/2010/node/403">Tools and Tips for KDE development</a><br /><em>Thomas McGuire</em><br /><a href="http://files.kde.org/akademy/2010/slides/Tools_and_Tips_for_KDE_Development-Thomas_McGuire.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/Tools_and_Tips_for_KDE_development-Thomas_McGuire.ogv"><img src="/media/2010/video.png" /></a></td>
<td class="track2"><a href="/2010/node/409">Porting KWin to Maemo/MeeGo</a><br /><em>Martin Gräßlin</em><br /><a href="http://files.kde.org/akademy/2010/slides/Porting_KWin_to_Maemo+MeeGo-Martin_Gräßlin.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/Porting_KWin_to_Maemo+MeeGo-Martin_Gräßlin.ogv"><img src="/media/2010/video.png" /></a></td>
</tr>
<tr>
<td>17:45 - 18:15</td>
<td class="track3"><a href="/2010/node/415">I can see your house from here</a><br /><em>Till Adam and Marius Bugge Monsen</em><br /><a href="http://files.kde.org/akademy/2010/slides/I_can_see_your_house_from_here-Till_Adam+Marius_Bugge_Monsen.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/I_can_see_your_house_from_here-Till_Adam_and_Marius_Bugge_Monsen.ogv"><img src="/media/2010/video.png" /></a></td>
<td class="track2"><a href="/2010/node/421">The KDE Plasma device spectrum: netbooks and mediacenters</a><br /><em>Alessandro Diaferia and Marco Martin</em><br /><a href="http://files.kde.org/akademy/2010/slides/The_KDE_Plasma_device_spectrum_netbooks_and_mediacenters-Alessandro_Diaferia+Marco_Martin.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/The_KDE_Plasma_device_spectrum_netbooks_and_mediacenters-Alessandro_Diaferia+Marco_Martin.ogv"><img src="/media/2010/video.png" /></a></td>
</tr>
<tr>
<td>18:15 - 18:45</td>
<td class="track3"><a href="/2010/node/427">Interview test: Testing the Model/View implementation in your application</a><br /><em>Stephen Kelly</em><br /><a href="http://files.kde.org/akademy/2010/slides/Interview_test_Testing_the_ModelView_implementation_in_your_application-Stephen_Kelly.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/Testing_the_Model_View_implementation_in_your_application-Stephen_Kelly.ogv"><img src="/media/2010/video.png" /></a></td>
<td class="track2"><a href="/2010/node/433">KDE Platform Profiles - Low fat software platform you can pick and choose from with sugar coating on top</a><br /><em>Kevin Ottens</em><br /><a href="http://files.kde.org/akademy/2010/slides/KDE_Platform_Profiles_-_Low_fat_software_platform_you_can_pick_and_choose_from_with_sugar_coating_on_top-Kevin_Ottens.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/KDE_Platform_Profiles_-_Low_fat_software_platform_you_can_pick_and_choose_from_with_sugar_coating_on_top-Kevin_Ottens.ogv"><img src="/media/2010/video.png" /></a></td>
</tr>
<tr>
<td>18:45 - 19:15</td>
<td class="track3"><a href="/2010/node/439">Boost for KDE Developers</a><br /><em>Volker Krause</em><br /><a href="http://files.kde.org/akademy/2010/slides/Boost_for_KDE_Developers-Volker_Krause.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/Boost_for_KDE_Developers-Volker_Krause.ogv"><img src="/media/2010/video.png" /></a></td>
<td class="track2"><a href="/2010/node/445">Toulouse University 4.0 - Fourth season great debriefing</a><br /><em>Benjamin Port, Rudy Commenge and Ludovic Deveaux</em><br /><a href="http://files.kde.org/akademy/2010/slides/Toulouse_University_4.0_-_Fourth_season_great_debriefing-Benjamin_Port.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/Toulouse_University_4.0_-_Fourth_season_great_debriefing-Benjamin_Port+Rudy_Commenge+Ludovic_Deveaux.ogv"><img src="/media/2010/video.png" /></a></td>
</tr>
</tbody>
</table>

<br>

<table id="sched_sun" class="schedule">
<tbody>
</tbody>
<tbody>
<tr>
<th colspan="3">Sunday, 4 July 2010</th>
</tr>
<tr>
<th>Time</th><th>Room 1</th><th>Room 2</th>
</tr>
<tr>
<td>&nbsp;</td>
<td class="track4"><strong>Applications</strong></td>
<td class="track1">&nbsp;</td>
</tr>
<tr>
<td>10:00 - 10:30</td>
<td class="track4"><a href="/2010/node/451">Working on Krita: Fun &amp; profit</a><br /><em>Lukas Tvrdy</em><br /><a href="http://files.kde.org/akademy/2010/slides/Working_on_Krita_Fun+profit-Lukas_Tvrdy.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/Working_on_Krita_Fun_and_profit-Lukas_Tvrdy.ogv"><img src="/media/2010/video.png" /></a></td>
<td class="track1"><a href="/2010/node/457">Managing Localization: What you should and shouldn't do</a><br /><em>Park Shinjo</em><br /><a href="http://files.kde.org/akademy/2010/slides/Managing_Localization_What_you_should_and_shouldnt_do-Park_Shinjo.pdf"><img src="/media/2010/pdf.png" /></a></td>
</tr>
<tr>
<td rowspan="5">10:30 - 11:00</td>
<td class="tracklightning" style="text-align: center;" colspan="2"><strong>Lightning talks</strong></td>
</tr>
<tr>
<td class="tracklightning"><a href="/2010/node/559">KDE Software for Scientists</a><br /><em>Stuart Jarvis</em><br /><a href="http://files.kde.org/akademy/2010/slides/KDE_Software_for_Scientists-Stuart_Jarvis.pdf"><img src="/media/2010/pdf.png" /></a></td>
<td class="tracklightning">KDE and the press<br /><em>Troy Unrau</em></td>
</tr>
<tr>
<td class="tracklightning"><a href="/2010/node/553">KDE's wikis - getting closer to world domination with SMW and SMW+</a><br /><em>Lydia Pintscher</em><br /><a href="http://files.kde.org/akademy/2010/slides/KDEs_wikis_-_getting_closer_to_world_domination_with_SMW_and_SMW+-Lydia_Pintscher.pdf"><img src="/media/2010/pdf.png" /></a></td>
<td class="tracklightning"><a href="/2010/node/547">KDE and the Secret Service</a><br /><em>Michael Leupold</em><br /><a href="http://files.kde.org/akademy/2010/slides/KDE_and_the_Secret_Service-Michael_Leupold.pdf"><img src="/media/2010/pdf.png" /></a></td>
</tr>
<tr>
<td class="tracklightning"><a href="/2010/node/565">UI Design for Developers</a><br /><em>Nuno Pinheiro and David Vignoni</em></td>
<td class="tracklightning"><a href="/2010/node/541">KStars showcase</a><br /><em>Akarsh Simha</em><br /><a href="http://files.kde.org/akademy/2010/slides/KStars_showcase-Akarsh_Simha.pdf"><img src="/media/2010/pdf.png" /></a></td>
</tr>
<tr>
<td class="tracklightning">Fluffy<br /><em>Frederik Gladhorn</em><br /><a href="http://files.kde.org/akademy/2010/slides/Fluffy-Frederik_Gladhorn.pdf"><img src="/media/2010/pdf.png" /></a></td>
<td class="tracklightning">&nbsp;</td>
</tr>
<tr>
<td>11:00 - 11:15</td>
<td class="notrack" colspan="2">Break</td>
</tr>
<tr>
<td>&nbsp;</td>
<td class="track4">&nbsp;</td>
<td class="track5"><strong>Cloud computing</strong></td>
</tr>
<tr>
<td>11:15 - 11:45</td>
<td class="track4"><a href="/2010/node/463">KDE performance again ... and again ... and again</a><br /><em>Luboš Luňák</em><br /><a href="http://files.kde.org/akademy/2010/slides/KDE_performance_again_and_again_and_again-Luboš_Luňák.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/KDE_performance_again_and_again_and_again-Luboš_Luňák.ogv"><img src="/media/2010/video.png" /></a></td>
<td class="track5"><a href="/2010/node/469">Unclouding ownCloud</a><br /><em>Frank Karlitschek</em><br /><a href="http://files.kde.org/akademy/2010/slides/Unclouding_owncloud-Frank_Karlitschek.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/Unclouding_ownCloud-Frank_Karlitschek.ogv"><img src="/media/2010/video.png" /></a></td>
</tr>
<tr>
<td>11:45 - 12:15</td>
<td class="track4"><a href="/2010/node/475">KDE Edu and how to learn by using KDE</a><br /><em>Aleix Pol</em><br /><a href="http://files.kde.org/akademy/2010/slides/KDE_Edu_and_how_to_learn_by_using_KDE-Aleix_Pol.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/KDE_Edu_and_how_to_learn_by_using_KDE-Aleix_Pol.ogv"><img src="/media/2010/video.png" /></a></td>
<td class="track5"><a href="/2010/node/493">A Better User Experience for the Web</a><br /><em>Sebastian Kügler</em><br /><a href="http://files.kde.org/akademy/2010/slides/A_Better_User_Experience_for_the_Web-Sebastian_Kügler.pdf"><img src="/media/2010/pdf.png" /></a></td>
</tr>
<tr>
<td>12:15 - 12:45</td>
<td class="track4">KDevelop4<br /><em>Milian Wolff & Aleix Pol</em><br /><a href="http://files.kde.org/akademy/2010/slides/KDevelop4-Milian_Wolff+Aleix_Pol.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/KDevelop4-Milian_Wolff_and_Aleix_Pol.ogv"><img src="/media/2010/video.png" /></a></td>
<td class="track5"><a href="/2010/node/481">Open Collaboration Services</a><br /><em>Frederik Gladhorn</em><br /><a href="http://files.kde.org/akademy/2010/slides/Open_Collaboration_Services-Frederik_Gladhorn.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/Open_Collaboration_Services-Frederik_Gladhorn.ogv"><img src="/media/2010/video.png" /></a></td>
</tr>
<tr>
<td>12:45 - 13:15</td>
<td class="notrack" colspan="2">Groupphoto</td>
</tr>
<tr>
<td>13:15 - 14:45</td>
<td class="notrack" colspan="2">Lunch</td>
</tr>
<tr>
<td>14:45 - 15:30</td>
<td class="track_keynote" colspan="2"><strong>Keynote</strong><br />Reaching For Greatness<br /><em>Aaron Seigo (KDE)</em><br /><a href="http://files.kde.org/akademy/2010/slides/Reaching_For_Greatness-Aaron_Seigo.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/Reaching_For_Greatness-Aaron_Seigo.ogv"><img src="/media/2010/video.png" /></a></td>
</tr>
<tr>
<td>&nbsp;</td>
<td class="track4">&nbsp;</td>
<td class="track6"><strong>Platforms</strong></td>
</tr>
<tr>
<td>15:30 - 16:00</td>
<td class="track4"><a href="/2010/node/499">The Directions of KOffice</a><br /><em>Inge Wallin</em><br /><a href="http://files.kde.org/akademy/2010/slides/The_Directions_Of_KOffice-Inge_Wallin.pdf"><img src="/media/2010/pdf.png" /></a></td>
<td class="track6"><a href="/2010/node/505">Beyond our comfort zone - spreading KDE software to non-free platforms</a><br /><em>Stuart Jarvis</em><br /><a href="http://files.kde.org/akademy/2010/slides/Beyond_our_comfort_zone_-_spreading_KDE_software_to_non-free_platforms-Stuart_Jarvis.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/Beyond_our_comfort_zone_-_spreading_KDE_software_to_non-free_platforms-Stuart_Jarvis.ogv"><img src="/media/2010/video.png" /></a></td>
</tr>
<tr>
<td>16:00 - 16:30</td>
<td class="track4"><a href="/2010/node/511">simon: Open Source Speech Recognition</a><br /><em>Peter Grasch</em><br /><a href="http://files.kde.org/akademy/2010/slides/Simon_Open_Source_Speech_Recognition-Peter_Grasch.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/Simon_Open_Source_Speech_Recognition-Peter_Grasch.ogv"><img src="/media/2010/video.png" /></a></td>
<td class="track6"><a href="/2010/node/517">How can you prepare your KDE application for Windows?</a><br /><em>Casper van Donderen</em><br /><a href="http://files.kde.org/akademy/2010/slides/How_can_you_prepare_your_KDE_application_for_Windows-Casper_van_Donderen.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/How_can_you_prepare_your_KDE_application_for_Windows-Casper_van_Donderen.ogv"><img src="/media/2010/video.png" /></a></td>
</tr>
<tr>
<td>16:30 - 16:45</td>
<td class="notrack" colspan="2">Break</td>
</tr>
<tr>
<td>16:45 - 17:30</td>
<td class="track4"><a href="/2010/node/523">Social Games</a><br /><em>Dan Leinir Turthra Jensen</em><br /><a href="http://files.kde.org/akademy/2010/slides/Social_Games-Dan_Leinir_Turthra_Jensen.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/Social_Games-Dan_Leinir_Turthra_Jensen.ogv"><img src="/media/2010/video.png" /></a></td>
<td class="track6"><a href="/2010/node/529">Chakra, A user-friendly distribution using the KDE desktop</a><br /><em>Laszlo Papp</em><br /><a href="http://files.kde.org/akademy/2010/slides/Chakra_A_user-friendly_distribution_using_the_KDE_desktop-Laszlo_Papp.pdf"><img src="/media/2010/pdf.png" /></a><a href="http://files.kde.org/akademy/2010/videos/Chakra_A_user-friendly_distribution_using_the_KDE_desktop-Laszlo_Papp.ogv"><img src="/media/2010/video.png" /></a></td>
</tr>
<tr>
<td>17:30 - 18:00</td>
<td class="track_keynote" colspan="2">Sponsor presentations</td>
</tr>
<tr>
<td>18:00 - 18:30</td>
<td class="notrack" colspan="2">Akademy awards ceremony and closing</td>
</tr>
</tbody>
</table>
