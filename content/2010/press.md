---
title:       "Press"
date:        2010-03-16
changed:     2016-11-02
menu:
  "2010":
    weight: 6
---
<img style="float: right;" src="/media/2010/press.png" alt="Press" width="232" height="120" />

<h3>Pressroom</h3>
<p>Members of the press can use a special press room at Akademy to prepare, write and do interviews. If needed interviews can be arranged.</p>
<p>Please contact Jos Poortvliet (details below) for inquiries if you want to talk to someone specifically.</p>

<h3>Press conferences</h3>

Saturday, 3rd July, at 13:00 a press conference will be arranged with the keynote speakers and some prominent KDE community members.

A second press conference discussing the events of the last week will take place on the last day of the conference, a time and place are still to be determined.

<h3>Information</h3>
<p>A press package with informational material will be available for people registered as journalists. It is recommended to mention in the comments area you're a journalist during registration, as we don't have a specific way to register this. You can also find press information about KDE on the <a title="KDE.org Presspage" href="http://www.kde.org/presspage/" target="_blank">KDE Presspage</a>.</p>
<h3>Contact</h3>
<p>For more information contact Jos Poortvliet (jospoortvliet AT KDE DOT  ORG)</p>

**More information for press, both visiting journalists and online visitors will be published soon!**
