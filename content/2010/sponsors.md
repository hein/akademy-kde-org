---
title:       "Sponsors"
date:        2010-04-02
changed:     2016-11-02
---

<h2>Platinum</h2>
<h3><a id="nokia" name="nokia"></a>Nokia</h3>
<p><strong><a href="http://nokia.com"><img style="float: right;" src="/media/2010/Nokia_Logo.png" alt="Nokia Logo" /></a></strong></p>

<p>At Nokia, we are committed to connecting people. We combine advanced technology with personalized services that enable people to stay close to what matters to them. Every day, more than 1.2 billion people connect to one another with a Nokia device – from mobile phones to advanced smartphones and high-performance mobile computers. Today, Nokia is integrating its devices with innovative services through Ovi (www.ovi.com), including music, maps, apps, email and more. Nokia's NAVTEQ is a leader in comprehensive digital mapping and navigation services, while Nokia Siemens Networks provides equipment, services and solutions for communications networks globally.</p>
<h4><a id="qt" name="qt"></a>Qt at Nokia</h4>
<p>A supporter of Akademy since its inception, Qt is a cross-platform application and UI framework designed to make developers' lives easier. With Qt you can efficiently develop advanced, web-enabled applications and user interfaces that run across desktop, mobile and embedded operating systems, without rewriting the source code. Industry leaders including Google, Skype, HP, Samsung and Asus use Qt to innovate and build on their market leadership.</p>

---

<h2>Gold</h2>
<h3><a id="intel" name="intel"></a>Intel</h3>
<p><strong><a href="http://oss.intel.com"><img style="float: right;" src="/media/2010/intel.png" alt="Intel Logo" width="267" height="176" /></a></strong></p>
<p>Intel's Open Source ethos runs deep. We offer deep technical expertise to the ecosystem, making contributions that enhance the performance, stability, and security of Open Source software. We also initiate communities like MeeGo and LessWatts.org that help foster innovation, and we participate in standards bodies to help move the industry forward. Because commercial products based on Open Source are an important part of the ecosystem, Intel works with those providers to optimize their solutions for the latest hardware features. As a result, end customers all over the world look to Intel® architecture as the platform of choice for Open Source solutions.</p>

---

<h2>Silver</h2>
<h3><a id="basyskom" name="basyskom"></a> basysKom</h3>
<p><strong><a href="https://www.basyskom.de/en/"><img style="float: right;" src="/media/2010/basyskom.med_.jpg" alt="Basyskom Logo" width="240" /></a></strong>basysKom - Greenhouse for Appealing Software</p>
<p>basysKom offers certified Qt consultancy and is fully committed to Qt and KDE technology. We are combining open source expertise with industry standard development methods, using Eva Brucherseifer's experience as former KDE president and our broad business knowledge to fulfill customer requirements.</p>
<p>basysKom is focused on development services for mobile clients, embedded devices and KDE technology. Our customers appreciate our German engineered and proven quality based on a certified Qt experience of more than 100 man-years. Today basysKom is the leading premium service supplier for high quality GUIs in DACH.</p>

<h3><a id="canonical" name="canonical"></a>Canonical</h3>
<p><a href="http://www.canonical.com/"><img style="float: right;" src="/media/2010/canonical_med.png" alt="Canonical Logo" width="240" height="30" /></a> As a member of the free software community, Canonical is committed to supporting the development of the open source ecosystem. While our main goal is to ensure that the Ubuntu family, including Kubuntu, continues to be freely available to everyone around the world, we are also focused on building new tools and software to encourage innovation. Projects like Upstart, Apport, Launchpad and Bazaar are just a few examples of our ongoing commitment to initiatives that promote software development and collaboration throughout the community.</p>

<h3><a id="collabora" name="collabora"></a> Collabora</h3>
<p><strong><a href="http://www.collabora.co.uk/"><img style="float: right;" src="/media/2010/collabora.med_.jpg" alt="Collabora Logo" width="240" height="79" /></a></strong>Collabora is a multinational Open Source consultancy specializing in bringing companies and the Open Source software community together. We give clients the knowledge, experience and infrastructure to allow them to become an integral part of the Open Source community.</p>
<p>We provide a full range of services based around Open Source technologies including architecture, software development, project management, infrastructure and community development. We are skilled in a wide range of areas from consumer devices to multimedia and real-time communications systems.</p>
<p>Collabora prides itself in creating a network of Open Source experts from all around the world</p>

<h3><a id="google" name="google"></a> Google</h3>
<p><strong><a href="http://www.google.com"><img style="float: right;" src="/media/2010/google.med_.jpg" alt="Google Logo" width="240" height="87" /></a></strong>Google's innovative search technologies connect millions of people around the world with information every day. Founded in 1998 by Stanford Ph.D. students Larry Page and Sergey Brin, Google today is a top web property in all major global markets.</p>
<p>Google's targeted advertising program provides businesses of all sizes with measurable results, while enhancing the overall web experience for users. Google is headquartered in Silicon Valley with offices throughout the Americas, Europe and Asia.</p>

<h3><a id="ixonos" name="ixonos"></a>Ixonos</h3>
<p><a href="http://www.ixonos.com/en"><img style="float: right;" src="/media/2010/ixonos.med_.jpg" alt="Ixonos Logo" width="240" height="39" /></a> Ixonos is an ICT services company creating innovative solutions for mobility, social media and digital services. Together with our customers we develop products and services which let people enjoy inspiring digital experience, anyplace, anytime. Our client organizations benefit from new business opportunities and new productivity.</p>
<p>There are more than 1000 of us and we are recognized for our customer orientation and responsible attitude. We work with passion with our colleagues, clients and partners alike.</p>

<h3><a id="opensuse" name="opensuse"></a>Open Suse</h3>
<p><a href="https://www.opensuse.org/"><img style="float: right;" src="/media/2010/opensuse_logo.png" alt="Ixonos Logo" /></a> The openSUSE project is a community program sponsored by Novell and developed in an open model. Promoting the use of Linux everywhere, openSUSE.org provides free, easy access to the world's most usable Linux distribution, openSUSE. The project gives Linux developers and enthusiasts everything they need to get started with Linux.</p>


---

<h2>Room sponsor</h2>
<h3><a id="nomovok" name="nomovok"></a> Nomovok</h3>
<p><a href="http://www.nomovok.com/"><img style="float: right;" src="/media/2010/nomovok.jpg" alt="Nomovok" width="240" /></a></p>
<p>NOMOVOK is specialized on Open Source deployments, specially focused on       MeeGo product creation services. Over 8 years the company has delivered       over 400 Open Source Software (OSS) deployments. NOMOVOK currently       employs over 100 OSS specialist, and collaborated tightly with 25       different leading OSS companies worldwide. Last years NOMOVOK has       heavily worked on QML and QT graphics pipeline optimization, to create       next generation UX.</p>

---

<h2>Coffee sponsor</h2>
<h3><a id="symbio" name="symbio"></a> Symbio</h3>
<p><a href="http://www.symbio.com/"><img style="float: right;" src="/media/2010/symbio.png" alt="Symbio" width="240" /></a></p>
<p>Symbio delivers software product engineering and product co-creation solutions across the entire product lifecycle. We provide expertise in developing products based on open source software, leveraging operating environments such as Red Hat, SuSE, MeeGo, Android, Wind River Linux and OpenEmbedded Linux. Companies like Nokia, ST-Ericsson, IBM, Vaisala and Neste Oil rely on Symbio as their co-creation partner.</p>

---

<h2>Media partner</h2>
<h3><a id="lmag" name="lmag"></a> Linux Magazin</h3>
<p><a href="http://www.linux-magazine.com/"><img style="float: right;" src="/media/2010/lmag.med_.jpg" alt="Linux Magazine" width="240" height="94" /></a></p>
<p>Linux Magazine is a monthly magazine serving readers in over 50 countries.</p>
<p>Linux Magazine is published in <a href="http://www.linux-magazine.com">English</a>, <a href="http://www.linux-magazin.de">German</a>, <a href="http://www.linux-magazine.es/">Spanish</a> and <a href="http://www.linux-magazine.pl/">Polish</a>.</p>

<h3 style="clear: both;"><a id="uuser" name="uuser"></a>Ubuntu User</h3>
<p><a style="float: right;" href="http://www.ubuntu-user.com/"><img src="/media/2010/ubuntuuser.med_.jpg" alt="Ubuntu User" width="240" height="73" /></a></p>
<p>Ubuntu User is the first print magazine specifically for Ubuntu's rapidly growing audience.</p>
<p>Ubuntu User is published in <a href="http://www.ubuntu-user.com/">English</a> and <a href="http://www.ubuntu-user.de/">German</a>.</p>
