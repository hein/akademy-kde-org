---
title:       "Akademy Attendee Policy"
date:        2012-06-19
---
KDE is dedicated to providing an enjoyable Akademy experience for everyone. As a community, we value and respect all people, regardless of gender, sexual orientation, race, ability, shape, size or preferred desktop environment. We will not tolerate vilification, abuse or harassment in any form.

<!--more-->

We appreciate that cultural differences may cause misunderstandings, so we will try to clarify these and smooth misunderstandings as they arise. But we also expect people to avoid playing dumb, and to make an effort to ensure a pleasant conference experience for everyone.

Conference participants behaving in ways that run counter to these principles risk being denied entrance or expelled from the conference at the discretion of the conference organizers. Conference speakers and other more visible figures especially should be aware of these concerns.

Harassment includes offensive verbal comments related to gender, sexual orientation, disability, physical appearance, body size, race, religion, sexual images in public spaces, deliberate intimidation, stalking, following, unauthorized or inappropriate photography or recording, sustained disruption of talks or other events, inappropriate physical contact, and unwelcome sexual attention. Participants asked to stop any harassing behavior are expected to comply immediately.

If a participant engages in harassing behavior, the conference organizers may take any action they deem appropriate, including warning the offender or expelling them from the conference. If you are being harassed, notice that someone else is being harassed, or have any other concerns, please contact a member of the conference team immediately. The conference team can be identified by their purple team t-shirts.

The conference team will be happy to help participants contact venue security or local law enforcement, provide escorts, or otherwise assist those experiencing harassment to feel safe for the duration of the conference. We value your attendance.

The organization team can be reached:
<ul>
<li>in person at the registration desk</li>
<li>by phone at (+372) 56 044 752 or (+372) 53 628 793</li>
<li>by email at akademy-team@kde.org</li>
</ul>

Additional phone numbers in case of an emergency:

- law enforcement: 110
- emergency/medical: 112
- taxi company:
  - Reval Takso, (+372) 6014600, base fare 2.50€, kilometer price 0.45€
  - Euro Takso, (+372) 6338888, base fare 1.98€, kilometer price 0.48€
  - Marabu Takso, (+372) 6500006, base fare 2.24€, kilometer price 0.45€

We expect participants to follow these guidelines and the <a href="http://www.kde.org/code-of-conduct/">KDE Code of Conduct</a> at all conference venues and conference-related social events. We are committed to a fun and productive conference for all attendees, and believe that this policy will help achieve that goal. We ask for the cooperation of all attendees in achieving our goal.
Thank you!
