---
title:       "Call For Volunteers"
date:        2012-06-01
changed:     2012-06-02
---
Are you attending Akademy 2012 in Tallinn? Are you interested in making the conference rock for attendees? Helping the Akademy Team organize everything? Do you want an exclusive free Akademy 2012 t-shirt?

Sign up as a volunteer. Help is needed for various tasks.

<!--more-->

<ul>
<li>Registration, pre-registration, information desk and merchandising</li>
<li>Prepare badges prior to the event</li>
<li>Help with video recording of sessions (training provided)</li>
<li>Be a Session Chair; introduce speakers and manage talk rooms</li>
<li>Set up conference rooms and other infrastructure such as Internet access</li>
<li>Runners</li>
<li>Doorkeepers for the social events (check badges, provide directions)</li>
<li>Press room staff</li>
<li>Other jobs that make Akademy run smoothly</li>
</ul>

If you want to help out, please add your name to the <a href= "http://community.kde.org/Akademy/2012#Volunteers">Volunteer Wiki page</a>, along with the times and days you are available, the languages you speak, and whether or not you are a local (or otherwise know Tallinn well).

If you are arriving early, there are also opportunities to help on Thursday, 28 June and Friday, 29 June.

Everybody attending the conference is welcome to to be an Akademy 2012 Volunteer. It doesn't matter where you are from, how old you are, whether you have ever attended a Free Software conference, or have ever been to Tallinn before. As long as you can communicate in English, you can contribute as a volunteer. For many people, being a volunteer is one of the high points of Akademy. This is an opportunity for you to make a difference for the KDE Community.

We'll do what's necessary to minimize the workload for everybody, but you should expect that your help will be needed for at least a couple of hours on a few days.

If you want to volunteer, please add your name to the <a href= "http://community.kde.org/Akademy/2012#Volunteers">Wiki page</a> before 15th June. This allows time to plan assignments. You are welcome to sign up later, but it would be of great help for you to do this before the 15th of June.

The KDE Community and the Akademy Team says, “Thank you to all volunteers in advance!”

If you have any questions, please contact akademy-team@kde.org or join us on #akademy on Libera Chat.
