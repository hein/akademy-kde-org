---
title:       "Sponsoring"
date:        2012-01-30
changed:     2012-02-03
menu:
  "2012":
    parent: sponsors
    weight: 2
---
<h2><i>Sponsor Akademy!</i></h2>

<p>At Akademy, we expect over 400 qualified attendees—software developers, members of the press, business people, government staff and educators from around the world. By sponsoring this event, you will receive substantial exposure in Europe, and will also reach top technical people elsewhere in the world.</p>


<p>The event will feature presentations on a range of important fields of interest within the ICT industry, including desktop development, application development, wireless, mobile and embedded device interfaces, multimedia, digital art, and Internet and web technology. The KDE Community has a history of innovations in each of these fields. As a sponsor, you have the opportunity to participate in this creative environment and be visible to the participants.</p>

<p>Akademy is not only a productive place to discuss technology. It also offers a chance for networking and creating friendships. The social events are possible entirely thanks to our sponsors. At these social events, people form relationships that sustain virtual teams and a successful, vibrant KDE community throughout the year. Akademy is an event where many bright people meet in a open and creative atmosphere, work hard, solve immediate challenges and come up with new ideas and solutions for the future. Sponsors are critical to the success of Akademy.</p>

<p><b>Your benefits as a sponsor of Akademy:</b></p>
<ul>
<li>Meet KDE key contributors, upstream and downstream maintainers, and users of one of the world's foremost User Interface technology platforms.</li>
<li>Meet other leading players. Besides the KDE community, many other Free Software projects and companies send delegates to Akademy.</li>
<li>Get inspired. Akademy provides an excellent environment for collecting feedback on new products, and for brainstorming new ideas.</li>
<li>Influence future product plans. At Akademy, the KDE project sets direction for the upcoming months. This is your chance to be among the first to hear of those plans, to discuss and influence them.</li>
<li>Be visible. A sponsorship gives you valuable promotional possibilities such as visible advertising, prominent talks, and international press coverage.</li>
<li>Encourage Free Software technologies. You support the development of new applications taking place in Akademy hackfests and intensive coding sessions.</li>
<li>Support Free Software. You will be recognized and enjoy publicity as a sponsor of the event. As importantly, you will also be supporting Free and Open Source Software, the KDE project, and the ideals that KDE is based on.</li>
<li>Be part of the KDE Community. KDE is one of the largest Free and Open Source Software communities in the world. It is dynamic, fun-loving, cooperative, committed, creative and hard working. For people associated with KDE, it is the ultimate expression of community. You will have direct access to the magic sauce that makes KDE so effective.</li>
</ul>

<p>Please see the <a href="/media/2012/SponsorshipBrochureInteractive.pdf">Sponsorship Opportunities Brochure</a> for full details of the options available.</p>

<b>Contact</b>

<p>If you would like more information or want to discuss sponsorship, please contact Claudia Rauch, rauch@kde.org.</p>
