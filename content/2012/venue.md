---
title:       "Venue"
date:        2012-01-30
changed:     2012-02-03
menu:
  "2012":
    weight: 4
---
## The Estonian IT College

<div style="float: right; padding: 1ex; margin: 1ex; border: 1px solid grey;"><img src="/media/2012/EstoniaITcollege350.jpeg" alt="Estonian IT College" /></div>

Akademy 2012 will be held at the Estonian IT College in Tallinn.

The Estonian Information Technology College is a private non-profit institution with more than 800 students. It was established in 2000 and is owned by the Estonian Information Technology Foundation.

<i>Address:  
IT College  
Raja 4C  
12616 Tallinn  
Estonia</i>

The College's mission is to offer the best applied IT education in the region, bringing together high-tech know-how and the practical needs of the information society, while being the axis of its development.

A third of all of the lecturers of the Estonian IT College come from IT firms and company or state IT departments. This helps to provide its students with balanced knowledge-based teaching and practice-oriented training. More information can be found <a href="http://www.itcollege.ee/en/it-college/">here</a>.
