---
title:       "Wiki"
date:        2012-04-05
menu:
  "2012":
    weight: 6
---
More information is available on <a href="http://community.kde.org/Akademy/2012">community.kde.org/Akademy/2012</a> and you are encouraged to add to it.
