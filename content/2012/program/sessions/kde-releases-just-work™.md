---
title:       "KDE Releases That Just Work™"
date:        2012-05-10
changed:     2012-05-13
aliases:     [ /2012/node/33 ]

---
<strong>Speaker:</strong> Jeroen van Meeuwen


A fast-paced development project involving many resources requires adaptive, evolutionary release engineering processes for facilitation, control and communication. These processes include scheduling, real-time tracking against schedule, and aligning with release goals. Issue tracking, quality assurance and source code management are additional key release management factors. Coordinating established processes and procedures has proven to be a daunting challenge on many occasions. But it's not impossible to achieve. KDE has attempted to streamline its release proceedings for the past decade or so. This presentation is intended to be a participatory conversation exploring how release engineering and quality assurance within the KDE project could be improved.
<p>&nbsp; </p>
<h2>Jeroen van Meeuwen</h2>
Jeroen van Meeuwen has been involved with a variety of release engineering teams across various projects including the Fedora Project. He is currently the Cyrus IMAP and Cyrus SASL release engineer, and is employed as a Systems Architect at Kolab Systems.

