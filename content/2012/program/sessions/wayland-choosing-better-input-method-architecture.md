---
title:       "Wayland: Choosing a Better Input Method Architecture"
date:        2012-05-10
changed:     2012-05-13
aliases:     [ /2012/node/46 ]

---
<strong>Speaker:</strong> Michael Hasselmann

This presentation will introduce the new input method system architecture for Wayland, highlighting the anticipated benefits for mobile platform integrators and input method developers. It will provide an overview of the adaptation work that is required by toolkit providers and what Wayland compositor implementations need to take care of in order to get the most out of this architecture.


There is a wide array of input methods for Linux. Very few have come close to the quality of popular input methods that are used for iOS or Android devices. This is partly due to platform and toolkit fragmentation in Linux. However, high-quality input methods have become a mandatory feature in the mobile world. A unified text input architecture that directly addresses the fragmentation challenge will attract developers of high-quality input methods and allow them to simultaneously target all platforms that support Wayland.

Such an architecture is feasible, and it will keep extra work for toolkit providers to a minimum, ultimately freeing them from having to provide their own custom input method systems.
<p>&nbsp; </p>
<h2>Michael Hasselmann</h2>
Michael Hasselmann has been working at <a href="http://www.openismus.com/" data-proofer-ignore>Openismus GmbH</a> since January 2009. He's been involved in the text input method team for Nokia's N9 since March 2010, helping to create one of the best virtual keyboards currently available in smartphones. Before that, he worked as a web developer for the Max-Delbrück-Centrum in Berlin.
