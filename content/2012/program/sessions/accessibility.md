---
title:       "Accessibility"
date:        2012-05-10
changed:     2012-05-13
aliases:     [ /2012/node/20 ]

---
<strong>Speaker:</strong> Frederik Gladhorn


KDE 4 has matured over the last few years to where it has reached a state that calls for incremental improvements and polish. Now a big goal could be to make KDE even more inclusive and welcoming for users with diverse backgrounds. In order to reach this goal, we need to make KDE accessible to users who need assistive technologies. Accessibility is a wide field and leads to improvements in applications and libraries for everyone. Having good accessibility provides benefits for everyone. Accessibility can reach from small utilities (such as RSI-break for repetitive strain injuries) to aids for users with hearing or visual impairments. KDE already does an excellent job in many areas, such as providing color schemes and font settings that go far beyond the usual and that allow adjustments to accommodate eye stress in various ways.

One area lacking effort is accommodation for blind users. KDE is well-suited, as it provides configureable keyboard shortcuts and usually enables good keyboard navigation. But a missing part is to make KDE work with screen-readers. A screen-reader would allow users with poor vision to explore their desktops through speech synthesis.

KDE's accessibility efforts picked up last year when the Qt AT-SPI bridge was resurrected. Now after one year the feedback is good. KDE enables people to access more technology and lets them participate in our project. There is still a lot to do. But since the basics are now in place, there are now opportunities for application developers to join in the effort. In this talk, I will summarize our progress, point out where help is needed and give some tips above improving applications with almost no additional effort.
<p>&nbsp; </p>
<h2>Frederik Gladhorn</h2>
Frederik Gladhorn is working on Qt for Nokia. Currently he focuses on making 
Qt accessibility a top-notch offering. He used to play with KHotNewStuff, KDE's framework to download additional content and widgets from various web sites. For learning languages, he cares about Parley in  the KDE Education Suite. Lately though he's been skiing cross-country instead of learning vocabulary.
