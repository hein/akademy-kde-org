---
title:       "Open Source Computer Vision Technology usable by people with Physical Disabilities"
date:        2012-05-10
changed:     2012-06-12
aliases:     [ /2012/node/41 ]

---
<strong>Speaker:</strong> Akarsh Sanghi


The KDE Accessibility Team works effortlessy not only towards making free software available to all but also to people who are physically challenged. One such tool is the Simon speech recognition which can be used to control the desktop environment with speech commands. I will be talking about how Simon has helped many people with such disabilities to increase their interaction with computers, and to encourage the use of free and open source software. There will be examples of how young students with physical disabilities have been using computers with KDE installed on them through Simon. I will show how computer vision technologies like face detection and recognition will make the software provided by the KDE community available to a larger audience. I conclude with how our efforts as open source contributors/developers can help society further in reaching out to a large number of people.
<p>&nbsp; </p>
<h2>Akarsh Sanghi</h2>
I am a student developer currently pursuing Computer Science Engineering from Jaypee Institute of Information Technology, Noida India. I have been involved in the KDE Community for the past 4 months and am loving the way it functions, and how people are willing to help. My main motive is to provide Open Source Computer Vision Technologies available to a wider audience especially for people with physical disabilities. I am a self-directed researcher and my interests and skills are completely aligned in the field of Human Computer Interaction, Artificial Intelligence and Computer Vision. I have been working on projects related to computer vision including the Kinect Sensor and have developed some expertise in the field.
