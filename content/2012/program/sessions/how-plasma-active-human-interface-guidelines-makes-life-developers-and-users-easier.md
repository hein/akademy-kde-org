---
title:       "How the Plasma Active Human Interface Guidelines Makes the Life of Developers and Users Easier"
date:        2012-05-10
changed:     2012-05-13
aliases:     [ /2012/node/28 ]

---
<strong>Speaker:</strong> Thomas Pfeiffer

One of the efforts of the interaction design team of Plasma Active is the development of Human Interface Guidelines (HIG) for Active Apps. Human Interface Guidelines serve mainly two important purposes: First, they support the creation of consistent applications on a platform, allowing users to transfer knowledge interacting with one application to other applications on the same platform and ensuring a harmonious look and feel across 
applications. 


Second, they allow application developers to apply proven interaction design principles without the need to ask an interaction designer about every common aspect of their UI. Thus, interaction designers and usability experts can focus on the uncommon, tricky parts of a specific application’s UI. And some standard and straightforward applications can be created without the need to involve interaction designers.

This presentation will introduce Plasma Active HIGs, give an overview of the existing HIGs and an outlook on those HIGs that are planned. After the presentation, developers will have the opportunity to comment on existing HIGs 
and suggest other HIGs they need.
<p>&nbsp; </p>
<h2>Thomas Pfeiffer</h2>
Thomas Pfeiffer joined the KDE community in 2008 as a participant in the "Season of Usability", creating KDE Human Interface Guidelines and Interaction Patterns, mentored by Celeste Lyn Paul and Ellen Reitmayr. After the Season of Usability, he provided usability consultancy and interaction design for Amarok and the Calligra Suite. In the summer of 2011, Thomas joined the Plasma Active team as interaction designer and usability / user-centered design consultant. He is a scientist working in the field of Usable Security at the Center for Advanced Security Research Darmstadt (CASED), after having worked in the software industry as product developer and manager for user-centered design for 2.5 years.
