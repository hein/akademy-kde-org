---
title:       "Qt in the Nokia Strategy - an Update"
date:        2012-05-10
changed:     2012-05-13
aliases:     [ /2012/node/39 ]

---
<strong>Speaker:</strong> Quim Gil

Nokia is the main driver of the Qt Project, assuming a big percentage of the development, shipping products based on Qt and advocating for the adoption of Qt in a wide range of industries. We will offer an update to the KDE Community stressing the points of collaboration and opportunities for KDE developers.
<p>&nbsp; </p>
<h2>Quim Gil</h2>
coming soon

