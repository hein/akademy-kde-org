---
title:       "Qt Project Update"
date:        2012-05-10
changed:     2012-05-13
aliases:     [ /2012/node/40 ]

---
<strong>Speaker:</strong> Thiago Macieira

The Qt Project has been up and running for months now. Contributors from KDE backgrounds have been great help in moving the project along, in a pure Open Source project way. This presentation will focus on how the project is set up and how KDE contributors can participate, as well as what's shaping up with Qt 5.


In an effort to become more open and to attract more developers, the Qt Project was established in 2011 to drive the future development of the project. A true Open Source project came to life in October with the launch of qt-project.org. An open governance model was established, based on models found in mature open source communities such as KDE, the Linux Kernel and WebKit. It has an easy entry path, a simple process to receive contributions and a group of maintainers responsible for the overall quality and direction.

The main goal of the Qt Project for 2012 is to launch Qt 5.0, the first major version of Qt in 7 years. Following the goals outlined last May, Qt 5.0 contains as its most important feature an overhaul of the graphics architecture, preparing it for the next several years of UX evolution.

This presentation will focus on how the Qt Project has matured in almost one year of existence and how KDE has helped that come to pass. For new contributors, it will present how to work with Qt 5.0, then move on to new features and changes in Qt 5.0. It will also review pending changes and plans for Qt 5.1, the release that the KDE Frameworks Project plans will be based on.
<p>&nbsp; </p>
<h2>Thiago Macieira</h2>
Thiago is the maintainer of QtCore and QtDBus, and was one of the main architects in creating the Qt Project. He is an experienced C++ developer, having spent the better part of the last 10 years developing Qt-based software, and Qt itself for the past 5. His last major project (in 2011) was the creation of the Qt Project, an open source project around the Qt codebase. He works for  Intel's Open Source Technology Center (OTC) from Oslo, Norway, where he splits his free time between his activities as the maintainer for Qt's QtCore and QtDBus libraries and his attempts at improving his StarCraft 2 skills.
