---
title:       "Building Accessible Systems: Accessibility in Practice"
date:        2012-05-10
changed:     2012-05-13
aliases:     [ /2012/node/22 ]

---
<strong>Speaker:</strong> Peter Grasch

For the last couple of years, the Simon Listens e.V. has been involved in a multitude of research projects to make a wide array of technology accessible for handicapped users and elderly people that is usually taken for granted.


We reached a decision early on to plan our projects according to the goals we wanted to achieve instead of focusing on the technology involved in reaching those goals. Software and hardware are means to an end, and should thus be interchangeable. But this approach leads to difficult questions: When to develop entirely new applications and when to adapt and extend existing ones for the special needs of a project? When does the need for a speed-to-market outweigh additional maintenance effort? How can recent innovations like declarative interfaces help to build accessible interfaces? Is it worthwhile to sacrifice maturity over features and ease of development?

Another seemingly simple question: What hardware is going to be involved? A big television screen is obviously a plus for many users but what about input devices? Are a mouse and keyboard really feasible? Is a touchscreen practical? Is voice recognition reliable?

In this talk, I will present two research projects that entailed developing systems for elderly and handicapped people. I will discuss how we addressed the questions outlined here and other issues that arose during the development and testing.
<p>&nbsp; </p>
<h2>Peter Grasch</h2>
I am the main developer behind the software Simon and Vice Chairman of the non-profit research organization Simon Listens e.V. I am pursuing a bachelors degree in computer science at the Graz University of Technology while working as the technical lead on the human-robot interaction part of the EU research project known as "Astromobile".
