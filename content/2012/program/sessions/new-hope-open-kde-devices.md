---
title:       "A New Hope: Open KDE Devices"
date:        2012-05-10
changed:     2012-05-12
aliases:     [ /2012/node/19 ]

---
<strong>Speaker:</strong> Aaron Seigo

KDE, along with other Free software communities, has waited for great open devices powered by Free software that allow anyone to participate and extend our reach and influence beyond the desktop. This presentation examines various efforts that are making this happen through direct, bottom-up action, focusing on the Make Play Live tablets and Add-ons Store as practical examples.



KDE has been producing versions of various software titles for mobile form factors: Plasma Active, Marble To Go, Calligra Active, Kontact Touch and more. However, unless these efforts end up on devices, they are merely exercises without much practical impact. 

In a perfect world, we would provide input into the design of devices which would carry KDE's software by default in an upstream-friendly manner. In this perfect world, a community that shares our ethics and ethos would encourage and aid others to develop software for such devices, and make it blend seamlessly with other offerings such as Plasma Desktop. 

This presentation is a look at how we are starting to build that "perfect world", with a focus on the Make Play Live devices. There will be a short review of progress to date, from negotiating for Free software 
compliance with Chinese companies to building out networks of community and corporate support. And then we'll describe where we want to go from here, hand-in-hand with KDE.

It will wrap up with "How To Get Involved"—writing applications (technologies to use, adapting desktop titles), entrepreneurial opportunities and how we can help each other realize our shared visions.
<p>&nbsp; </p>
<h2>Aaron Seigo</h2>
Aaron Seigo is a KDE Community member who has contributed by writing software (e.g. Plasma), providing community coordination, giving presentations at events and interviews with media around the world. He has served as a Board member and President of KDE e.V. Building on more than a decade with KDE and twenty years in the industry, Aaron is now focused on opening new doors to KDE outside the traditional desktop form factor.
