---
title:       "How to Make Your Commit Seen"
date:        2012-05-10
changed:     2012-05-13
aliases:     [ /2012/node/29 ]

---
<strong>Speaker:</strong> Marta Rybczyńska

KDE Commit Digest is a weekly overview of KDE development activity based on commits in KDE repositories. The digest is created by a team that reviews and classifies the commits. 

The main goal of the talk is to create better understanding between developers and the Digest Team so that we can help each other better. Another important topic will be ways to use the digest to reach out to the whole KDE Community and even outside of it.


In the technical part of the presentation, we will describe the digest workflow and point out things that help people make the best use of it. We will explain how commits are chosen to be included in the digest, give hints on the preferred commit message content, show special tags that can point to important changes, offer suggestions on writing featured articles, and more.

The second part of the talk will be devoted to the community role of the Digest—informing and promoting. We will give ideas about how to use the Commit Digest to gain new developers and promote them, how to promote new interesting projects and how to gain new users.

The session will be interactive for the Digest Team to get ideas about what developers want. We also want to get feedback about how we can help the Community grow.
<p>&nbsp; </p>
<h2>Marta Rybczyńska</h2>
In her professional life, Marta Rybczyńska is an embedded software expert. She has been working deep inside embedded kernels like Linux and various Real-Time Operating systems, in system libraries and frameworks up to user interfaces. In addition, she has been working on Polish KDE translation for 10 years and provides Linux support on different forums.
