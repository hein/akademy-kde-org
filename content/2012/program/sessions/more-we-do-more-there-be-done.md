---
title:       "The more we do the more there is to be done"
date:        2012-05-10
changed:     2012-06-20
aliases:     [ /2012/node/43 ]

---
<strong>Speaker:</strong> Nuno Pinheiro and Martin Zilz


KDE 3.5 marked the end of an era in which design in KDE was mostly made up of uncoordinated efforts. It worked okay in its time. In the 4.X series, we presented Oxygen. And for the first time, there was a complete set of themes for all theming angles in the KDE Experience. The state we are in today is far better than where we were 5 years ago. But many of our Design vulnerabilities were exposed along the way. For the future, Oxygen needs to be expanded into app UI development itself. New tools such as Qt Quick demand the experience of Visual Designers.
   
In this talk, Nuno and Martin will share their experiences, tips, tricks, demos, real live code examples and opinions. They will discuss how to nurture an environment where different people with different skill sets help each other make FOSS apps prettier and more awesome.
<p>&nbsp; </p>
<h2>Nuno Pinheiro, Martin Zilz</h2>
Nuno Pinheiro is a Senior Visual Designer living in Portugal. He works for Klarälvdalens Datakonsult AB (KDAB) developing UIs and UXes. He has been a KDE contributor since 2004. Nuno works in the KDE Artist Team and is the current coordinator of the Oxygen project.

Martin Zilz is co-founder of "kreativkonzentrat", a webdesign agency based in Germany. He has a passion for User Interface design and has been involved in the Oxygen project since 2009. Apart from being a designer, Martin also loves to code.
