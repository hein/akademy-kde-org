---
title:       "How Contour became Plasma Active: The next step of the mobile platform"
date:        2012-05-10
changed:     2012-06-15
aliases:     [ /2012/node/27 ]

---
<strong>Speaker:</strong> Eva Brucherseifer 

Many of the core concepts of the Plasma Active User Experience (UX) were designed and created in a research project called Contour. Learn more about the original goals of Contour in 2010, how the Community joined the Project and how it became Plasma Active.


The talk will give insights about the core ideas and concepts behind Plasma Active, the UI evolution of Contour as the main UX and will highlight the UI design process in the Contour project. Those UX concepts were realized in an embedded development process, which was new ground for KDE. With this base, Plasma Active started its journey and evolved its potential.
<p>&nbsp; </p>
<h2>Eva Brucherseifer</h2>
Eva is a long term KDE contributor. In the early days, she was President of KDE e.V., designing the basic structure of the KDE e.V. organization and bootstrapping Akademy. With her love for innovative ideas, she was part of the Appeal meetings which kickstarted basic KDE4 ideas. Now she is a co-founder and a leader of the Plasma Active project. In her daily life, Eva runs the embedded services company basysKom, which also offers Qt consulting, Plasma Active productization and customization, and concept design for mobile business cases.

