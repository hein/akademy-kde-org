---
title:       "KDE Frameworks 5: The Community Experiment"
date:        2012-05-10
changed:     2012-05-12
aliases:     [ /2012/node/32 ]

---
<strong>Speaker:</strong> Kevin Ottens

A new on-going effort within KDE was announced last year. It is intended to create a new software offering called KDE Frameworks which will replace the good old "kdelibs" and "KDE Platform". Lots has been said and written about the motives and technical aspects behind the upcoming KDE Frameworks 5 (KF5). In this talk, we'll be exploring the community aspects instead, focusing on the impact of the new interactions within and around KDE.


After a short introduction, we will try to analyze and understand three big changes the work on KF5 brought to our community.

First, the success of the KDE Frameworks approach is strongly tied to the way we interact with our upstreams. We will see how our relationships with them has changed. The obvious upstream here is the Qt Project where the landscape has changed forever. We'll also talk about the way we work with CMake.

Second, one of the (often forgotten) original motives of KDE Frameworks was not technical in any way but completely about people. This aspect had two sides: the people working on our former platform, and the people consuming it (in or out of the KDE community). We'll show how KDE Frameworks is already changing the interactions between those two groups.

Third, because of the humongous task at hand, which involves new technical outcomes never produced before, new techniques were needed to drive those changes. We'll then cover some of the practices we put in place to push forward the development of KDE Frameworks. There will be particular focus on the ones inspired by the agile software development community, and the ones aimed at growing the KDE Frameworks community.

This talk will conclude with an overall picture on the progress made during the past year toward the goal of releasing KF5.

<h2>Kevin Ottens</h2>
Kevin Ottens is a long term hacker in the KDE community. He contributes to the KDE community at large, with a strong emphasis on API design and frameworks architecture. He has a PhD in computer science which led him to concentrate on ontologies engineering and multi-agent systems. Kevin's job at KDAB includes developing research projects around KDE technologies. He still lives in Toulouse where he serves as a part-time teacher at his former university.
