---
title:       "Pre Registration Event"
date:        2012-06-20
changed:     2012-12-19
menu:
  "2012":
    parent: program
    weight: 2
---
<div style="float: right; padding: 1ex; margin: 1ex;">
<img src="/media/2012/IntelSide.png" />
<br />Sponsored by Intel
</div>

Location: <a href="http://keller.edicypages.com/kontakt" data-proofer-ignore>Bayern Böhmen Keller</a>  
Address: Suur-Karja 18, 10146 Tallinn  
Date: Friday, 29 June 2012  
Time: 15:00-20:00  
Sponsor: Intel Open Source Technology Center  

As many of you will be arriving on Friday, there will be a pre-registration event taking place at the Bayern Böhmen Keller. Here, you’ll be able to enjoy drinks and snacks sponsored by our welcome reception sponsor, Intel, and meet other Akademy attendees. You’ll also be able to complete your registration here in order to avoid the crowd on Saturday morning.

<!--more-->
