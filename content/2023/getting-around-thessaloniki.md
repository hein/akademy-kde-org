---
title: Getting around Thessaloniki
menu:
  "2023":
    parent: travel
    weight: 3
hideSponsors: true
layout: single-in-container  # remove this when removing hideSponsors
---
**Bus lines to/from the venue, University of Macedonia** (*[https://oasth.gr/en](https://oasth.gr/en "https\://oasth.gr/en")*)

The University of Macedonia is conveniently located in the city centre, with the following bus stops in 2’ min walking distance:

* Ag. Fotini bus stop (C’ September str, near the University ). Lines 10, 31, 01X
* University of Macedonia bus stop (in front of the University entrance, Egnatia str.): Lines 02K, 07, 14, 58
* University of Macedonia bus stop (across the University, Egnatia str): Lines 01X, 02K, 14

**Public Transportation**

 Purchase (*there are ticket vending machines on the bus that only accept coins and do not give change so make sure you have the exact amount on you!) and validate your ticket by touching it to the pad on the turnstile before boarding a bus or train. Inspectors randomly check for tickets. If you do not have a ticket, have the wrong ticket, or fail to validate your ticket, you could be fined up to 60 times the basic fare.

**Short-term Car Rentals**

Greek law requires that visitors carry a valid U.S. driver’s license and an international driver’s permit (IDP), even if the rental company does not request to see your IDP. The Embassy does not issue IDPs. You must obtain your IDP in the United States from the American Automobile Association (AAA) or the American Automobile Touring Alliance (AATA). Contact AAA directly to inquire about their mail in option that will allow you to apply from overseas, but note that it will likely take days or weeks. If you drive without these documents, you may face a very high fine (1,000 Euros or more) or be responsible for all expenses in the event of an accident.

**Road Conditions and Safety**

Greece has one of the highest traffic fatality rates in the European Union. Exercise extreme caution both as a driver and a pedestrian.
