---
title: Program Overview
menu:
  "2023":
    parent: details
    weight: 1
hideSponsors: true
layout: single-in-container  # remove this when removing hideSponsors
---
<p>
 <div class="row py-5">
    <div class="col-lg-10 mx-auto">
      <div class="card rounded shadow border-0">
        <div class="card-body p-5 rounded">
          <div class="table-responsive">
<!--table border=1 style="border-top-style: solid; border-top-color: #ccc; border-top-width: 2px;"-->
<!--table id="program" style="width:100%" class="table table-striped table-bordered"-->
<table id="program" class="table table-striped  table-bordered">
<tr>
<th>Day</th>
<th>Morning</th>
<th>Afternoon</th>
<th>Evening</th>
</tr>
<tr>
<td>Fri 14th</td>
<td colspan=2><center>&nbsp;<center></td>
<td><center><!--a href="https://akademy.kde.org/2023/welcome-event-friday"-->Welcome Event<!--/a--></center></td>
</tr>
<tr>
<td>Sat 15th</td>
<td colspan=2><center><!--a href="https://conf.kde.org/event/4/timetable/#20221001"-->Talks Day 1<!--/a--></center></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Sun 16th</td>
<td colspan=2><center><!--a href="https://conf.kde.org/event/4/timetable/#20221002"-->Talks Day 2<!--/a--></center></td>
<td><center><!--a href="https://akademy.kde.org/2022/social-event-sunday"-->Social Event<!--/a--></center></td>
</tr>
<tr>
<td>Mon 17th</td>
<td colspan=2><center><!--a href="https://community.kde.org/Akademy/2023/Monday"-->BoFs, Workshops & Meetings<!--/a--><br /><a href="https://ev.kde.org/generalassembly/">KDE eV AGM</a> (1100-1400)</center></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Tue 18th</td>
<td colspan=2><center><!--a href="https://community.kde.org/Akademy/2023/Tuesday"-->BoFs, Workshops & Meetings<!--/a--></center></td>
<td></td>
</tr>
<tr>
<td>Wed 19th</td>
<!--td><center><a href="https://community.kde.org/Akademy/2023/Wednesday">BoFs, Workshops & Meetings</a></center><--/td-->
<td colspan=3><center><!--a href="https://akademy.kde.org/2023/daytrip-wednesday"-->Daytrip<!--/a--></center></td>
</tr>
<tr>
<td>Thu 20th</td>
<td colspan=2><center><!--a href="https://community.kde.org/Akademy/2023/Thursday"-->Trainings and BoFs, Workshops & Meetings<!--/a--></center></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Fri 21st</td>
<td colspan=2><center><!--a href="https://community.kde.org/Akademy/2023/Friday"-->BoFs, Workshops & Meetings<!--/a--></center></td>
<td>&nbsp;</td>
</tr>
</table>
          </div>
        </div>
      </div>
    </div>
  </div>
</p>
